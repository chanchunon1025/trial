# Guideline on converting clearcase AIX resource to cloud resource

## Prerequisite

To proceed, several prerequisites must be met.

**Setting up ClearCase for AIX resources**

Either have a dynamic view or a snapshot view. However, it is recommended to have a dynamic view installed as it makes the file retrieval process more straightforward. 

**Setting up a Git repository for cloud resources.**

To clone a remote repository to your local machine using GitLab, follow these steps:

1. Log in to your GitLab account and navigate to the TAAS project
2. Click on the **Clone** button and copy the URL provided.
3. Open your terminal or Git Bash and navigate to the directory where you want to clone the repository.
4. Run the following command: `git clone <repository URL>`

**Installing cloud converter**

Go to Q:\IR1E\cloud_migration\cloud_convertor.zip and unzip it on your computer.

**Installing Beyond Compare**

Go to Q:\IR1E\cloud_migration\tools\beyond_compare.zip and unzip it on your computer.

**Assigning a GitLab TAAS project maintainer role**

If you don't have GitLab TAAS project maintainer role, ask someone with access to grant it to you. Contact the maintainer and request access. 

**CCMP++ Admin role**

To process the production request, you must have the CCMP++ administrator right.

## To deploy a change to the non-production environment

1. In GitLab, check out a new working branch from the PRD branch.
2. Pull the remote change to your local Git workspace.
3. Switch to the local working branch.
4. Download the list of files from ClearCase.
5. Convert the list of files using the cloud converter.
6. Compare the converted files with the ones in the Git repository.
7. Merge the two versions if they contain the IRM-172 label on the cloud side.
8. Sync the merged version to the cloud repository.
9. Add, commit, and push the change from the local working branch to the remote working branch
10. In GitLab, create a merge request from the remote working branch to the non-production target branch 
11. Review the merge request details and submit the merge request.
12. Wait for the merge request pre-compilation pipeline to succeed.
13. Merge the changes.
14. Wait for the target branch compilation.
15. CCMP++ will be automatically created.
16. Input the artifacts URL to the CCMP++ request
17. Trace the CCMP++ request status: Q (Queuing) → P (Processing) → W (Waiting Transfer) → T (Transferring) → C (Completed)

## deploy a change to cloud production environment

1. In GitLab, check out a new working branch from the PRD branch.
2. Pull the remote change to your local Git workspace.
3. Switch to the local working branch.
4. Download the list of files from ClearCase.
5. Convert the list of files using the cloud converter.
6. Compare the converted files with the ones in the Git repository.
7. Merge the two versions if they contain the IRM-172 label on the cloud side.
8. Sync the merged version to the cloud repository.
9. Add, commit, and push the change from the local working branch to the remote working branch
10. In GitLab, create a merge request from the remote working branch to the production branch 
11. Review the merge request details and submit the merge request.
12. CCMP++ will be automatically created.
13. Wait for the merge request pre-compilation pipeline to succeed.
14. Create TSR for manager approval
15. Merge the changes after TSR endosement
16. Wait for the target branch compilation.
17. Input the TSR No. to the CCMP++ request
18. Input the artifacts URL to the CCMP++ request
19. Trace the CCMP++ request status: S (Submitted) → Q (Queuing) → P (Processing) → W (Waiting Transfer) → L (Transfer Locked) → T (Transferring) → C (Completed)

## deploy a hotfix to cloud production environment

1. In GitLab, check out a new MTN branch from the rel_PRD tag
2. In GitLab, check out a new working branch from the rel_PRD/rel_MTN tag
3. Pull the remote change to your local Git workspace.
4. Switch to the local working branch.
5. Download the list of files from ClearCase.
6. Convert the list of files using the cloud converter.
7. Compare the converted files with the ones in the Git repository.
8. Merge the two versions if they contain the IRM-172 label on the cloud side.
9. Sync the merged version to the cloud repository.
10. Add, commit, and push the change from the local working branch to the remote working branch
11. In GitLab, create a merge request from the remote working branch to the MTN branch 
12. Review the merge request details and submit the merge request.
13. Wait for the merge request pre-compilation pipeline to succeed.
14. Create TSR for manager approval
15. (Optional) Message to the manager requesting approval for overnight support.
16. Merge the changes after Manager endosement
17. Wait for the target branch compilation.
18. CCMP++ will be automatically created.
19. Input the TSR No. to the CCMP++ request
20. Input the artifacts URL to the CCMP++ request
21. Trace the CCMP++ request status:  Q (Queuing) → P (Processing) → W (Waiting Transfer) → T (Transferring) → C (Completed)

## Additional notes

- If the GitLab merge pre-compilation fails, simply go to the relevant build console log to trace the root cause. There are three common types of errors that may occur:
1. Compilation error: This occurs when the file changes result in a compilation error. The application team will need to fix the error.
2. Build script error: If the file changes require dependency setup, such as packaging relocation setup, please reach out to Joseph for assistance with updating the build script.
3. Jenkins server error: In the event that too many builds occur at once, the Jenkins server may throw an error. In such cases, simply retry the build.

- To rebuild a merge request, simply enter the command "Jenkins please rebuild" in the merge request comment. Afterward, refresh the page and you will see the pipeline loading.
- To rebuild any failed build in Jenkins, navigate to the relevant build page and select the rebuild option
- After each successful build, the program's packaging location will be indicated in a 'relocation.txt' file. This will assist in identifying the target jar for deployment.
- Once CCMP++ has completed the deployment, a release tag will be added to the relevant commit in GitLab. You can use this tag to identify the deployed version.

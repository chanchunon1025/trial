# Mybatis - Nullable Field with Default Values

This chapter offers sample test cases and practical insights on how MyBatis manages nullable fields with default values when inserting a new record into relevant table.

In the following section, two methods of insertion will be discussed.

Using the "insert" approach:

1. Set the fields of the Java model to null, then execute insert method in Dao.
2. Do not set the fields of the Java model, then execute insert method in Dao.

Using "insertSelective" approach

- assume those testing columns exist in the database table only (the table should contain one column, says RECORD_ID). They do not exist in the Java model.

exist only in the database table and not in the Java model

1. in the sql xml file, add checking of existence to those new fields. Then execute insert in Dao.

# Sample

xxxDao

```java
public void insert(TmpCtfaps1aNullable record) {
    getSqlMapClientTemplate().insert("TmpCtfaps1aNullable.insert", record);
}
```

```java
public void insertSelective(TmpCtfaps1aNullable record) {
    getSqlMapClientTemplate().insert("TmpCtfaps1aNullable.insertSelective", record);
}
```

Java model

```java
public class TmpCtfaps1aNullableKey {
    private String injobName;

    private Short injobRunSeq;

    private Date createTs;

    private Long rowNum;

    public String getInjobName() {
        return injobName;
    }

    public void setInjobName(String injobName) {
        this.injobName = injobName;
    }

    public Short getInjobRunSeq() {
        return injobRunSeq;
    }

    public void setInjobRunSeq(Short injobRunSeq) {
        this.injobRunSeq = injobRunSeq;
    }

    public Date getCreateTs() {
        return createTs;
    }

    public void setCreateTs(Date createTs) {
        this.createTs = createTs;
    }

    public Long getRowNum() {
        return rowNum;
    }

    public void setRowNum(Long rowNum) {
        this.rowNum = rowNum;
    }
}
```

XML

```xml
<insert id="insert" parameterType="ird.taas2.batch.ct.model.TmpCtfaps1aNullable">
  insert into TMP_CTFAPS1A (INJOB_NAME, INJOB_RUN_SEQ, CREATE_TS, PRN)
  values (#{injobName,jdbcType=VARCHAR}, #{injobRunSeq,jdbcType=SMALLINT}, #{createTs,jdbcType=TIMESTAMP}, #{prn,jdbcType=CHAR})
</insert>
```

XML

```xml
<insert id="insertSelective" parameterType="ird.taas2.batch.ct.model.TmpCtfaps1aNullable">
  insert into TMP_CTFAPS1A
  <trim prefix="(" suffix=")" suffixOverrides=",">
    <if test="injobName != null">
      INJOB_NAME,
    </if>
    <if test="injobRunSeq != null">
      INJOB_RUN_SEQ,
    </if>
    <if test="createTs != null">
      CREATE_TS,
    </if>
    <if test="prn != null">
      PRN,
    </if>
  </trim>
  <trim prefix="values (" suffix=")" suffixOverrides=",">
    <if test="injobName != null">
      #{injobName,jdbcType=VARCHAR},
    </if>
    <if test="injobRunSeq != null">
      #{injobRunSeq,jdbcType=SMALLINT},
    </if>
    <if test="createTs != null">
      #{createTs,jdbcType=TIMESTAMP},
    </if>
    <if test="prn != null">
      #{prn,jdbcType=CHAR},
    </if>
  </trim>
</insert>
```

# Testing Result

[TmpCtfaps1aNullableDaoImpl.java](Mybatis%20-%20Nullable%20Field%20with%20Default%20Values%20c9540bd3772c41c2b1806189e1100296/TmpCtfaps1aNullableDaoImpl.java)

[TmpCtfaps1aNullableDao.java](Mybatis%20-%20Nullable%20Field%20with%20Default%20Values%20c9540bd3772c41c2b1806189e1100296/TmpCtfaps1aNullableDao.java)

[TmpCtfaps1aNullable.xml](Mybatis%20-%20Nullable%20Field%20with%20Default%20Values%20c9540bd3772c41c2b1806189e1100296/TmpCtfaps1aNullable.xml)

[TmpCtfaps1aNullableExample.java](Mybatis%20-%20Nullable%20Field%20with%20Default%20Values%20c9540bd3772c41c2b1806189e1100296/TmpCtfaps1aNullableExample.java)

[TmpCtfaps1aNullableKey.java](Mybatis%20-%20Nullable%20Field%20with%20Default%20Values%20c9540bd3772c41c2b1806189e1100296/TmpCtfaps1aNullableKey.java)

[TmpCtfaps1aNullable.java](Mybatis%20-%20Nullable%20Field%20with%20Default%20Values%20c9540bd3772c41c2b1806189e1100296/TmpCtfaps1aNullable.java)

Study Result

Nullable declaration (with or without default value) only affect insertselective() behavior.

mybatis insert() method will not affect from nullable declaration, in term of null value will not replaced by default value.

Nullable declaration with default value under insert() only avoid err code -407 not address to null handling

My view for the using insert() and insertSelective is as below, you may further verify with your team.
(I) When using insert() with a nullable field, there are situations that null value is created to the record when using with native iBatis/myBatis generated DAO(pls refer to email from Joe/1B below). We have to either amend the DAO or add the default value assignment to the new field for the insert(). Therefore your statement " (4) the new Nullable field could be defined under DDL with default value for which all SQL without referencing to the field could automatically assigned with the default value" is not correct.
On the other hand, if we use non-null, the program will abend but it avoid the null value created to the record.

(II) An example of using insertSelective() with a not-null fields. your statement " (2) the 'Insert Selective' statement has to hardcode each data field" is not correct.
(The below example illustrate the situations that there are business rules/conditions of some program variables to determine the value of the DB Table data field before insertion of record, and the non-nullable value will ultimately be safeguarded by the DDL even an insert() is used.  It seems not relevant to my statement of the 'Insert Selective' statement has to hardcode each data field"  )
(IIa) When a new java Ctchchrge is created, all fields in the java object are null by default
(IIb) if we do not amend the program to explicitly assign a value, the new data item will be kept as null when calling insertSelective.
(IIc) at the DAO(XML) layer, the null value of the field will be kept until record insertion.
<if test="provPrevSetOff != null   ">,
PROV_PREV_SET_OFF
</if>
(IId) the DDL definition will be triggered and assigned with default value when the record is created onto the table
"PROV_PREV_SET_OFF" DECIMAL(11,2) NOT NULL WITH DEFAULT 0 ,

(III) the amount of programs that use insert() only or using both method is per table basis. It is up to the programmer's action at the time he/she create the DAO for the table. So it can not be generally concluded that our program use insert() more than insertSelectiv().
(The problem is not there are insert() statement being used more than insertSelective(),  the issue is we definitely have insert() commonly used in current program.   And both insert() and  insertSelective()  have its shortcoming, and if the option to Define in DDL using Non-nullable with default value does not have risk and could avoid the un-necessary change of functions that are not related to the newly added data field, it should be the option.    Price and Judy have not done enough to perform the study, I've instructed them to study the Option to defined DDL using Non-nullable with default value for  both  Insert()  and  insertSelective(), as well as the DAO(XML) layer.      May be your team could provide more relevant samples.

(IV) for the new eDDA field in the CT Collection File, I will keep the current DDL definition of not-null and the current DAO coding.
(CT are having different situation from PF as you don't have insert() statement for the particular table, and definitely it is the most prudent choice to adopt non-null.     Given that there are no other affected function, we would have your agreement to implement the database change and TC05 in June 2023.)